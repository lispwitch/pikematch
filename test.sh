#!/bin/mksh


test_regex=('^a*'
            '^a*'
            'ba'
            'ba$'
            'ba*'
            '.*')

test_strns=('aaaaaa'
            'baaaaa'
            'ba'
            'abba'
            'baaaaa'
            'asdf')

for i in ${!test_regex[*]};
do
	regex=${test_regex[$i]}
	str=${test_strns[$i]}

	echo "input: \"$regex\" \"$str\""

	./pikematch "$regex" "$str"
	echo "pike: $?"
	
	./match "$regex" "$str"
	echo "match: $?"

	./assembly "$regex" "$str"
	echo "assembly: $?"

	echo ""
done
