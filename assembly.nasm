section .text
global _start

_start:
	mov rdi, qword [rsp+16]
	mov rsi, qword [rsp+24]
	cmp qword rdi, 0
	je .exit
	cmp qword rsi, 0
	je .exit

	call match
	mov rdi, rax
	mov rax, 60
	syscall

.exit:
	mov rax, 60
	mov rdi, 2
	syscall


match: ; rdi = regex, rsi = s
	cmp byte [rdi], '^'
	jne .loop
	inc rdi
	call matchc
	ret

.loop:
	cmp byte [rsi], 0
	je .exit_bad
	call matchc
	cmp rax, 1
	je .exit_good
	inc rsi
	jmp .loop

.exit_bad:
	mov rax, 0
	ret

.exit_good:
	mov rax, 1
	ret


matchc: ; rdi = regex, rsi = s
	xor rbx, rbx
.loop:
	cmp byte [rdi], 0
	je .exit_good
	cmp byte [rdi], '$'
	je .exit_end
	cmp byte [rsi], 0
	je .exit_bad

	movsx rbx, byte [rdi+1]
	cmp bl, '*'
	je .match_star_setup

	movsx rbx, byte [rdi]

	cmp byte [rdi], '.'
	je .continue
	cmp byte [rsi], bl
	jne .exit_bad

.continue:
	inc rdi
	inc rsi
	jmp .loop

.match_star_setup:
	add rdi, 2
.match_star:
	cmp byte [rsi], 0
	je .exit_bad
	push rbx
	call matchc
	pop rbx
	cmp rax, 1
	je .exit_good
	cmp byte [rsi], '.'
	jne .exit_bad
	cmp byte [rsi], bl
	jne .exit_bad
	inc rsi
	jmp .match_star


.exit_end:
	cmp byte [rsi], 0
	je .exit_good
.exit_bad:
	mov rax, 0
	ret
	
.exit_good:
	mov rax, 1
	ret
