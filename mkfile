
CFILE=`{ls *.c}
ASMFILE=`{ls *.nasm}

do:Q: $CFILE $ASMFILE
  :

%.nasm:V:
  nasm -f elf64 -g -F dwarf $target -o $stem.o
  ld -m elf_x86_64 $stem.o -o $stem

%.c:V:
  gcc -g $target -o $stem

clean:V:
  rm -f match pikematch assembly *.o


